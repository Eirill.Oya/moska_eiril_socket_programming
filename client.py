import socket

SERVER_NAME = 'localhost'
PORT = 5050
sock = socket.socket() #TCP SOCKET

sock.connect(("localhost", PORT))

def get_role():
    sock.send("Role".encode())
    role = sock.recv(1024).decode()
    return role
def get_advice():
    sock.send("Advice".encode())
    role = sock.recv(1024).decode()
    return role
def get_situation():
    sock.send("Situation".encode())
    response = sock.recv(1024).decode()
    return response

while True:
    input("Press ENTER to start")
    role = get_role()
    print(role)
    #sock.sendall(b' ROLL')
    #_, role = sock.recv(2024).decode().split(" ", 1)
    print(f"You are {role}")
    if role == "advisee":
        situasjon = input("Enter situation: ")
        advice = get_advice()
        sock.send(situasjon.encode())
        print(advice)
    elif role == "advisor": 
        situasjon = get_situation()
        print(f"Situsation: {situasjon}")
        advice = input("Enter advice: ")
        sock.sendall(f'{advice}'.encode())
        print(sock.recv(2024).decode())

        